    var oldone = null;

    function show(id) {
        document.getElementById(id).classList.remove('hidden');
    }

    function hide(id) {
        document.getElementById(id).classList.add('hidden');

    }

    function toggle(oldone, id) {
        hide(oldone);
        show(id);
        oldone = id;

    }

    function defaultSection(){
        show('about');
    }

    // FIXME call show('about') on page load
    document.onload = defaultSection;
