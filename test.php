<?php

function check_email_address($email) {
    // check @ symbol and lengths are right.
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        return false;
    }

  // Split it into sections
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&
            ↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$",
            $local_array[$i])) {
          return false;
        }
    }

  // Check if domain is IP. If not, it should be valid domain name
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
        $domain_array = explode(".", $email_array[1]);

        if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
        }

        for ($i = 0; $i < sizeof($domain_array); $i++) {

            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|
                ↪([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }

    return true;
}

    $name = $REQUEST['name'];
    $email = $REQUEST['email'];
    $message = $REQUEST['message'];

    $code_guessed = false;

    if ($name) {

        //Email present and valid
        if ($email && check_email_address($email)) { 
            //If everything looked ok, send the contact form content by email to a defined address
            $destination = "txusinho@gmail.com";
            $subj = "The form data";
            $headers = "From: ". $from;
            $mail_sent = mail($destination, $subj, message );


             //BONUS: PHP) add some very basic logging system;
            if(!$mail_sent){
                error_log("Email not sent From " . $from . ".\n To "
                    . $destination ." with the content " . $message);
            }
        }

   } 
    if (!$code_guessed) {

?>

<!-- // ensure STRICT XHTML compliance -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Tasks for Percona</title>
    <script scr="http://ajax.googleapis.com/ajax/libs/jquery/1.4.99/jquery.min.js"></script> 

    <style>
    .hidden {
        display: none;
        visiblity: hidden;
        weight: 0px;
    }
    </style>

    <script type="text/javascript">

    var oldone = null;

    function show(id) {
        document.getElementById(id).classList.remove('hidden');
    }

    function hide(id) {
        document.getElementById(id).classList.add('hidden');

    }

    function toggle(oldone, id) {
        hide(oldone);
        show(id);
        oldone = id;

    }

    function defaultSection(){
        show('about');
    }

    // FIXME call show('about') on page load
    document.onload(defaultSection);
    </script>

</head>

<body>

    <?php   //  FIXME: Make this title change when switching between sections

            //  BONUS: CSS) use a nice font for this title. 

    <h1>Example</h1>

    <div>

<?php

    // BONUS: CSS) make this an horizontal menu with rounded buttons ; JS) make the transition between sections look really nice

    echo "<ul>"

    print '<li><a onclick="javascript:void(toggle("about"))" href="#about">about</a>';
    print '<li><a onclick="javascript:void(toggle("services"))" href="#services">services</a>';
    print '<li><a onclick="javascript:void(toggle("customers"))" href="#customers">customers</a>';
    print '<li><a onclick="javascript:void(toggle("contacts"))" href="#contacts">contacts</a>';

    echo "</ul>";

?>

    </div><br><br><br>

    

<?php

    // BONUS: DESIGN/CSS) improve how the section contents looks

    echo '<div id="about" class="hidden"><!-- // -></div>';

    echo '<div id="services" class="hidden"><!- // --></div>';

    echo '<div id="customers" class="hidden"><!-- // -></div>';

    echo '<div id="contacts" class="hidden"><!- // --></div>';

?>    



<?php

    if (...) { 

    // FIXME when url === #contacts, 

    // Display a form with three parameters: 

            // - Name (Text Input, 25 chars max), 

            // - Email(Text Input, 25 chars with obligatory @ sign), 

            // - Message (Text Area, 255 chars) 

    // Add simple antispam protection (anyone you propose // please explain why)

    // If parameters are wrong, the form must be pre-populated with submitted data

    // BONUS: JS/CSS) make the form look awesome



        print '

        ';

    }

?>

</body>

</html>

<?php

    } // end $code_guessed 